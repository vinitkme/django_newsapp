newsapp
=======

News App Using Django CMS pages.

This app works on the belief that you have a news page and there are sub-pages with different news. Also, make sure you have added news as id the parent news page.

In order to use this app, do these things:
- Include the app name in the installed apps in your base.py settings
```
{{project-name}}.apps.newsapp
```

- Include the tags in your template where you want to insert this, in my implementation it works as a sidebar. You can use it however you like. 
```
{% load page_news %} // on the top
{% get_newsitems %} // where you want the sidebar to be injected.
```

Hope this works for you!
