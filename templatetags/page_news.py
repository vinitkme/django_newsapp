from django import template
from cms.models.pagemodel import Page

register = template.Library()

@register.inclusion_tag('page_news/news.html')
def get_newsitems():
    news_page = Page.objects.filter(reverse_id='news')
    news_page_child = news_page.get().children.all()
    print news_page_child
    return {
            'newsitems': news_page_child
    }
